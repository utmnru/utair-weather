import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

const openweathermap_url = 'https://api.openweathermap.org/data/2.5/'
const openweathermap_appid = '6dce70a1a2223ed5598b1677f79ca032'

const { localStorageObject, removeFromLocalStorageObject } = require('./helper')

export default new Vuex.Store({
  state: {
    weather_data: localStorage.getItem('weather_data') || {},
    error: ''
  },
  mutations: {
    PUSH_WEATHER_DATA(state, payload){
      state.weather_data = payload
    },
    SET_ERROR(state, payload){
      state.error = payload
    },
    CLEAR_ERROR(state){
      state.error = ''
    },

  },
  actions: {
    GET_WEATHER_DATA_BY_COORDS({commit}, coords){
      axios.get(openweathermap_url + 'weather?lat=' + coords.lat + '&lon=' + coords.lon + '&appid=' + openweathermap_appid + '&units=metric').then(res => {
        //console.log('weather by coords: ', res.data)

        localStorageObject(res.data)

        commit('CLEAR_ERROR')

      }, error => {
        console.log(error)

        commit('SET_ERROR', 'Not data. Please check you internet connection.')
      })
    },
    GET_WEATHER_DATA_BY_PLACE({commit}, place){
      axios.get(openweathermap_url + 'weather?q=' + place + '&appid=' + openweathermap_appid + '&units=metric').then(res => {
        //console.log('weather by place: ', res.data)

        localStorageObject(res.data)

        commit('CLEAR_ERROR')

      }, error => {
        console.log(error)

        commit('SET_ERROR', 'Place is incorrect. Please input correct place name.')

      })
    },
    GET_WEATHER_DATA_BY_ID({commit}, id){
      axios.get(openweathermap_url + 'weather?id=' + id + '&appid=' + openweathermap_appid + '&units=metric').then(res => {
        //console.log('weather by id: ', res.data)

        localStorageObject(res.data)

        commit('CLEAR_ERROR')

      }, error => {
        console.log(error)

        //commit('SET_ERROR', 'Place ID is incorrect. Please input correct place ID.')

      })
    },
    REMOVE_PLACE_FROM_WEATHER({}, key){

        removeFromLocalStorageObject(key)

    }
  },
  getters: {
    getWeatherData: (state) => state.weather_data,
    getError: (state) => state.error,
  }
})