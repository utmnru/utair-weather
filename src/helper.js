import store from './store'
import Vue from 'vue'

export function localStorageObject (data) {

    if(localStorage.getItem('weather_data')){
      if(!localStorage.getItem('weather_data').hasOwnProperty("key")){
        addToLocalStorageObject('weather_data', data.id, data)
      }
    }else{
      addToLocalStorageObject('weather_data', data.id, data)
    }

    store.commit('PUSH_WEATHER_DATA', localStorage.getItem('weather_data'))

}
  
export function addToLocalStorageObject (name, key, value) {
  
    let existing = localStorage.getItem(name);
      existing = existing ? JSON.parse(existing) : {};
      existing[key] = value;
      localStorage.setItem(name, JSON.stringify(existing));
  
}

export function removeFromLocalStorageObject (key) {

  let existing = localStorage.getItem('weather_data');
    existing = existing ? JSON.parse(existing) : {};


    if(existing.hasOwnProperty(key)){

      Vue.delete(existing, key)

      //localStorage.removeItem('weather_data')
     
      localStorage.setItem('weather_data', JSON.stringify(existing))

      store.commit('PUSH_WEATHER_DATA', localStorage.getItem('weather_data'))
    }else{
      console.log('Key is not found')
    }

}